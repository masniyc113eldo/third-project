class Visit {
    constructor(params) {
        this.goal = params.goal;
        this.shortProp = params.shortProp;
        this.urgency = params.urgency;
        this.name = params.name
    }
}

class dentist extends Visit {
    constructor(params) {
        super(params);
        this.lastVisit = params.lastVisit;
    }
}

class therapist extends Visit {
    constructor(params) {
        super(params);
        this.age = params.age;
    }
}

class cardiologist extends Visit {
    constructor(params) {
        super(params);
        this.pressure = params.pressure,
            this.index = params.index,
            this.diseases = params.diseases,
            this.age = params.age
    }
}


const doctorHTML = document.querySelector('.doctor-name');

const modalCardiologist = document.querySelector('.modal-cardiologist')
const modalDentist = document.querySelector('.modal-dentist')
const modal = document.querySelector('.modal')
const cancelBTN = document.querySelector('.cancel-btn')
const confirmBTN = document.querySelector('.confirm-btn')

let doctor;

doctorHTML.addEventListener('change', function () {
    const doctor = doctorHTML.value;
    console.log(doctor);
    if (doctor === 'dentist') {
        modalCardiologist.style.display = 'none';
        modalDentist.style.display = 'flex'
        ageHTML.style.display = 'none'
    } else if (doctor === 'cardiologist') {
        modalCardiologist.style.display = 'flex'
        ageHTML.style.display = 'flex'
        modalDentist.style.display = 'none'
    } else if (doctor === 'therapist') {
        ageHTML.style.display = 'flex'
        modalDentist.style.display = 'none'
        modalCardiologist.style.display = 'none'
    }
});


function closeModal() {
    modal.style.display = 'none'
}

function openModal() {
    modal.style.display = 'flex';
}

const addCardBtn = document.querySelector('.add-card');


addCardBtn.addEventListener('click', function () {
    openModal()
    console.log('Button clicked');
});


openModal()


const ageHTML = document.querySelector('.modal-age')
const nameHTML = document.querySelector('.modal-name')
const pressureHTML = document.querySelector('.modal-pressure')
const urgencyHTML = document.querySelector('.modal-urgency')
const descriptionHTML = document.querySelector('.modal-shortdescription')
const goalHTML = document.querySelector('.modal-goal')
const diseasesHTML = document.querySelector('.modal-diseases')
const lastVisitHTML = document.querySelector('.modal-last-visit')
const indexHTML = document.querySelector('.modal-index')

cancelBTN.addEventListener('click', function () {
    closeModal()
})

document.addEventListener('click', function (event) {
    if (event.target.closest('.modal') === null) {
        closeModal();
    }
});

let card;


confirmBTN.addEventListener('click', function () {
    const doctorSelected = eval(doctorHTML.value)
    if (doctorHTML.value !== 'dentist' && doctorHTML.value !== 'cardiologist' && doctorHTML.value !== 'therapist') {
        alert('Choose the doctor')
        return
    }
    const card = new doctorSelected({
        age: ageHTML.value,
        name: nameHTML.value,
        pressure: pressureHTML.value,
        shortProp: descriptionHTML.value,
        goal: goalHTML.value,
        urgency: urgencyHTML.value,
        index: indexHTML.value,
        lastVisit: lastVisitHTML.value,
        diseases: diseasesHTML.value
    })


    if (card.age === '' ||
        card.name === '' ||
        card.pressure === '' ||
        card.goal === '' ||
        card.urgency === '' ||
        card.index === '' ||
        card.lastVisit === '' ||
        card.diseases === '') {
        alert('Fill all the data')
        return;
    }
    createCard(card)

    fetch('https://ajax.test-danit.com/api/v2/cards', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer 97c776c6-dc4b-46b5-8ee2-9aaf5d892cc6`
        },
        body: JSON.stringify(card),
    })
        .then(response => response.json())
        .then(data => {
            closeModal()
            console.log(data);
        })
        .catch((error) => {
            console.error('Error:', error);
        });;
})




const cardList = document.querySelector('.card-list');
const createdCard = document.createElement('div');
createdCard.classList.add('card');
const deleteBtn = document.createElement('i')
deleteBtn.classList.add('fa-solid', 'fa-xmark', 'delete-btn')
const showMoreBtn = document.createElement('button')
showMoreBtn.classList.add('show-more-btn')
const shortCreatedInfo = document.createElement('div');
shortCreatedInfo.classList.add('short-card-info');
const longCreatedInfo = document.createElement('div');
longCreatedInfo.classList.add('full-card-info');
showMoreBtn.innerHTML = 'Show more'



function createCard(card) {
    let doctorCreated;

    if (doctorHTML.value === 'dentist') {
        doctorCreated = 'Dentist';
        longCreatedInfo.innerHTML = `<h2>Goal: ${card.goal}</h2>
        <h2>Urgency: ${card.urgency}</h2>
        <h2>Description: ${card.shortProp}</h2>
        <h2>Last visit: ${card.lastVisit}</h2>`;
    } else if (doctorHTML.value === 'therapist') {
        doctorCreated = 'Therapist';
        longCreatedInfo.innerHTML = `
        <h2>${card.goal}</h2>
        <h2>Urgency: ${card.urgency}</h2>
        <h2>Description: ${card.shortProp}</h2>
        <h2>Age: ${card.age}</h2>`;
    } else if (doctorHTML.value === 'cardiologist') {
        doctorCreated = 'Cardiologist';
        longCreatedInfo.innerHTML = `
        <h2>${card.goal}</h2>
        <h2>Urgency: ${card.urgency}</h2>
        <h2>Description: ${card.shortProp}</h2>
        <h2>Pressure: ${card.pressure}</h2>
        <h2>Body mass index: ${card.index}</h2>
        <h2>Diseases: ${card.diseases}</h2>
        <h2>Age: ${card.age}</h2>`;
    }

    shortCreatedInfo.innerHTML = `
    <h2>${doctorCreated}</h2>
    <h2>${card.name}</h2>`;


    cardList.append(createdCard);
    createdCard.append(deleteBtn)
    createdCard.append(shortCreatedInfo);
    createdCard.append(longCreatedInfo);
    createdCard.append(showMoreBtn)
    const emptyList = document.querySelector('.empty-list')
    const cards = document.querySelectorAll('.card');
    if (cards.length < 1) {
        emptyList.style.display = 'flex'
    } else if (cards.length > 0) {
        emptyList.style.display = 'none'
    }

    cardList.append(createdCard);
}
showMoreBtn.addEventListener('click', function () {
    const fullInfo = createdCard.querySelector('.full-card-info');
    const allCards = document.querySelectorAll('.card');




    allCards.forEach(function (otherCard) {
        if (otherCard !== createdCard) {
            otherCard.classList.remove('show-less');
            otherCard.style.height = '150px';
            otherCard.querySelector('.full-card-info').style.display = 'none';
            otherCard.querySelector('.show-more-btn').innerHTML = 'Show more';
        }
    });

    if (createdCard.classList.contains('show-less')) {
        createdCard.classList.remove('show-less');
        fullInfo.style.display = 'none';
        createdCard.style.height = '150px';
        showMoreBtn.innerHTML = 'Show more';
    } else {
        createdCard.classList.add('show-less');
        fullInfo.style.display = 'flex';
        createdCard.style.height = 'auto';
        showMoreBtn.innerHTML = 'Show less';
    }
});

